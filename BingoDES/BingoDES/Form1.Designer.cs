﻿using System;

namespace BingoDES
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbQua = new System.Windows.Forms.Label();
            this.lbTer = new System.Windows.Forms.Label();
            this.lbSeg = new System.Windows.Forms.Label();
            this.lbPri = new System.Windows.Forms.Label();
            this.lbAtual = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.gbx2 = new System.Windows.Forms.GroupBox();
            this.bt54 = new System.Windows.Forms.Button();
            this.bt28 = new System.Windows.Forms.Button();
            this.bt53 = new System.Windows.Forms.Button();
            this.bt50 = new System.Windows.Forms.Button();
            this.bt52 = new System.Windows.Forms.Button();
            this.bt49 = new System.Windows.Forms.Button();
            this.bt51 = new System.Windows.Forms.Button();
            this.bt40 = new System.Windows.Forms.Button();
            this.bt45 = new System.Windows.Forms.Button();
            this.bt48 = new System.Windows.Forms.Button();
            this.bt44 = new System.Windows.Forms.Button();
            this.bt47 = new System.Windows.Forms.Button();
            this.bt43 = new System.Windows.Forms.Button();
            this.bt46 = new System.Windows.Forms.Button();
            this.bt42 = new System.Windows.Forms.Button();
            this.bt41 = new System.Windows.Forms.Button();
            this.bt36 = new System.Windows.Forms.Button();
            this.bt39 = new System.Windows.Forms.Button();
            this.bt35 = new System.Windows.Forms.Button();
            this.bt38 = new System.Windows.Forms.Button();
            this.bt34 = new System.Windows.Forms.Button();
            this.bt37 = new System.Windows.Forms.Button();
            this.bt33 = new System.Windows.Forms.Button();
            this.bt32 = new System.Windows.Forms.Button();
            this.bt31 = new System.Windows.Forms.Button();
            this.bt30 = new System.Windows.Forms.Button();
            this.bt29 = new System.Windows.Forms.Button();
            this.gbx1 = new System.Windows.Forms.GroupBox();
            this.bt1 = new System.Windows.Forms.Button();
            this.bt27 = new System.Windows.Forms.Button();
            this.bt26 = new System.Windows.Forms.Button();
            this.bt25 = new System.Windows.Forms.Button();
            this.bt24 = new System.Windows.Forms.Button();
            this.bt18 = new System.Windows.Forms.Button();
            this.bt17 = new System.Windows.Forms.Button();
            this.bt16 = new System.Windows.Forms.Button();
            this.bt15 = new System.Windows.Forms.Button();
            this.bt9 = new System.Windows.Forms.Button();
            this.bt8 = new System.Windows.Forms.Button();
            this.bt7 = new System.Windows.Forms.Button();
            this.bt6 = new System.Windows.Forms.Button();
            this.bt23 = new System.Windows.Forms.Button();
            this.bt22 = new System.Windows.Forms.Button();
            this.bt13 = new System.Windows.Forms.Button();
            this.bt21 = new System.Windows.Forms.Button();
            this.bt20 = new System.Windows.Forms.Button();
            this.bt19 = new System.Windows.Forms.Button();
            this.bt14 = new System.Windows.Forms.Button();
            this.bt12 = new System.Windows.Forms.Button();
            this.bt11 = new System.Windows.Forms.Button();
            this.bt10 = new System.Windows.Forms.Button();
            this.bt5 = new System.Windows.Forms.Button();
            this.bt4 = new System.Windows.Forms.Button();
            this.bt3 = new System.Windows.Forms.Button();
            this.bt2 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.easyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mediumToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.difficultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblsaldo = new System.Windows.Forms.Label();
            this.addMoneyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.add500ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.add1000ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.add1500ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.gbx2.SuspendLayout();
            this.gbx1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.SpringGreen;
            this.groupBox1.Controls.Add(this.lbQua);
            this.groupBox1.Controls.Add(this.lbTer);
            this.groupBox1.Controls.Add(this.lbSeg);
            this.groupBox1.Controls.Add(this.lbPri);
            this.groupBox1.Controls.Add(this.lbAtual);
            this.groupBox1.Controls.Add(this.pictureBox9);
            this.groupBox1.Controls.Add(this.pictureBox8);
            this.groupBox1.Controls.Add(this.pictureBox7);
            this.groupBox1.Controls.Add(this.pictureBox6);
            this.groupBox1.Controls.Add(this.pictureBox4);
            this.groupBox1.Location = new System.Drawing.Point(446, 64);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(368, 112);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ultimos Numeros";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // lbQua
            // 
            this.lbQua.AutoSize = true;
            this.lbQua.BackColor = System.Drawing.Color.White;
            this.lbQua.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbQua.Location = new System.Drawing.Point(25, 57);
            this.lbQua.Name = "lbQua";
            this.lbQua.Size = new System.Drawing.Size(0, 16);
            this.lbQua.TabIndex = 16;
            // 
            // lbTer
            // 
            this.lbTer.AutoSize = true;
            this.lbTer.BackColor = System.Drawing.Color.White;
            this.lbTer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTer.Location = new System.Drawing.Point(89, 57);
            this.lbTer.Name = "lbTer";
            this.lbTer.Size = new System.Drawing.Size(0, 16);
            this.lbTer.TabIndex = 15;
            // 
            // lbSeg
            // 
            this.lbSeg.AutoSize = true;
            this.lbSeg.BackColor = System.Drawing.Color.White;
            this.lbSeg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSeg.Location = new System.Drawing.Point(153, 57);
            this.lbSeg.Name = "lbSeg";
            this.lbSeg.Size = new System.Drawing.Size(0, 16);
            this.lbSeg.TabIndex = 14;
            // 
            // lbPri
            // 
            this.lbPri.AutoSize = true;
            this.lbPri.BackColor = System.Drawing.Color.White;
            this.lbPri.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPri.Location = new System.Drawing.Point(217, 57);
            this.lbPri.Name = "lbPri";
            this.lbPri.Size = new System.Drawing.Size(0, 16);
            this.lbPri.TabIndex = 13;
            // 
            // lbAtual
            // 
            this.lbAtual.AutoSize = true;
            this.lbAtual.BackColor = System.Drawing.Color.White;
            this.lbAtual.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAtual.Location = new System.Drawing.Point(293, 39);
            this.lbAtual.Name = "lbAtual";
            this.lbAtual.Size = new System.Drawing.Size(0, 24);
            this.lbAtual.TabIndex = 12;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackgroundImage = global::BingoDES.Properties.Resources.red;
            this.pictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox9.Location = new System.Drawing.Point(6, 38);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(60, 50);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 11;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackgroundImage = global::BingoDES.Properties.Resources.yellow;
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox8.Location = new System.Drawing.Point(72, 42);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(58, 46);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 10;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImage = global::BingoDES.Properties.Resources.blue;
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox7.Location = new System.Drawing.Point(136, 42);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(58, 46);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 9;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImage = global::BingoDES.Properties.Resources.orange1;
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(196, 38);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(62, 50);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 8;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = global::BingoDES.Properties.Resources.rose1;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(264, 13);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(92, 75);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 6;
            this.pictureBox4.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.pictureBox3);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.pictureBox5);
            this.groupBox2.Controls.Add(this.pictureBox10);
            this.groupBox2.Controls.Add(this.pictureBox11);
            this.groupBox2.ForeColor = System.Drawing.Color.ForestGreen;
            this.groupBox2.Location = new System.Drawing.Point(12, 65);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(237, 457);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "On-line";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(171, 378);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(26, 17);
            this.label18.TabIndex = 17;
            this.label18.Text = "15";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(171, 233);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(26, 17);
            this.label17.TabIndex = 16;
            this.label17.Text = "15";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(171, 82);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(26, 17);
            this.label16.TabIndex = 15;
            this.label16.Text = "15";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(150, 357);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(70, 13);
            this.label15.TabIndex = 14;
            this.label15.Text = "Remaining:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(150, 213);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "Remaining:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(150, 61);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "Remaining:";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Lime;
            this.label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(87, 357);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(46, 31);
            this.label12.TabIndex = 8;
            this.label12.Text = "99";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Lime;
            this.label11.Cursor = System.Windows.Forms.Cursors.Default;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(87, 213);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 31);
            this.label11.TabIndex = 7;
            this.label11.Text = "99";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Lime;
            this.label10.Cursor = System.Windows.Forms.Cursors.Default;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(87, 61);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 31);
            this.label10.TabIndex = 6;
            this.label10.Text = "99";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(27, 403);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "gucci";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(31, 261);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Jeny";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(17, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Guest32";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::BingoDES.Properties.Resources.Gianluca_avatar__1_1;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(17, 341);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(59, 59);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::BingoDES.Properties.Resources.Gianluca_avatar;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(20, 199);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(56, 59);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::BingoDES.Properties.Resources.a1;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(20, 45);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(56, 59);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.MintCream;
            this.pictureBox5.Location = new System.Drawing.Point(7, 37);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(225, 92);
            this.pictureBox5.TabIndex = 10;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.MintCream;
            this.pictureBox10.Location = new System.Drawing.Point(6, 189);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(225, 92);
            this.pictureBox10.TabIndex = 11;
            this.pictureBox10.TabStop = false;
            this.pictureBox10.Visible = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.MintCream;
            this.pictureBox11.Location = new System.Drawing.Point(6, 331);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(225, 93);
            this.pictureBox11.TabIndex = 12;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Snow;
            this.label2.Location = new System.Drawing.Point(288, 193);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 44);
            this.label2.TabIndex = 0;
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.SpringGreen;
            this.groupBox4.Controls.Add(this.gbx2);
            this.groupBox4.Controls.Add(this.gbx1);
            this.groupBox4.Location = new System.Drawing.Point(446, 182);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(368, 340);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Cartões";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // gbx2
            // 
            this.gbx2.Controls.Add(this.bt54);
            this.gbx2.Controls.Add(this.bt28);
            this.gbx2.Controls.Add(this.bt53);
            this.gbx2.Controls.Add(this.bt50);
            this.gbx2.Controls.Add(this.bt52);
            this.gbx2.Controls.Add(this.bt49);
            this.gbx2.Controls.Add(this.bt51);
            this.gbx2.Controls.Add(this.bt40);
            this.gbx2.Controls.Add(this.bt45);
            this.gbx2.Controls.Add(this.bt48);
            this.gbx2.Controls.Add(this.bt44);
            this.gbx2.Controls.Add(this.bt47);
            this.gbx2.Controls.Add(this.bt43);
            this.gbx2.Controls.Add(this.bt46);
            this.gbx2.Controls.Add(this.bt42);
            this.gbx2.Controls.Add(this.bt41);
            this.gbx2.Controls.Add(this.bt36);
            this.gbx2.Controls.Add(this.bt39);
            this.gbx2.Controls.Add(this.bt35);
            this.gbx2.Controls.Add(this.bt38);
            this.gbx2.Controls.Add(this.bt34);
            this.gbx2.Controls.Add(this.bt37);
            this.gbx2.Controls.Add(this.bt33);
            this.gbx2.Controls.Add(this.bt32);
            this.gbx2.Controls.Add(this.bt31);
            this.gbx2.Controls.Add(this.bt30);
            this.gbx2.Controls.Add(this.bt29);
            this.gbx2.Location = new System.Drawing.Point(0, 170);
            this.gbx2.Name = "gbx2";
            this.gbx2.Size = new System.Drawing.Size(355, 164);
            this.gbx2.TabIndex = 17;
            this.gbx2.TabStop = false;
            this.gbx2.Text = "Cartão 2";
            this.gbx2.Visible = false;
            // 
            // bt54
            // 
            this.bt54.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt54.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt54.ForeColor = System.Drawing.Color.Black;
            this.bt54.Location = new System.Drawing.Point(310, 112);
            this.bt54.Name = "bt54";
            this.bt54.Size = new System.Drawing.Size(40, 48);
            this.bt54.TabIndex = 40;
            this.bt54.UseVisualStyleBackColor = false;
            this.bt54.Click += new System.EventHandler(this.bt54_Click);
            // 
            // bt28
            // 
            this.bt28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt28.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt28.ForeColor = System.Drawing.Color.Black;
            this.bt28.Location = new System.Drawing.Point(6, 20);
            this.bt28.Name = "bt28";
            this.bt28.Size = new System.Drawing.Size(40, 48);
            this.bt28.TabIndex = 18;
            this.bt28.UseVisualStyleBackColor = false;
            this.bt28.Click += new System.EventHandler(this.bt28_Click_1);
            // 
            // bt53
            // 
            this.bt53.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt53.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt53.ForeColor = System.Drawing.Color.Black;
            this.bt53.Location = new System.Drawing.Point(272, 112);
            this.bt53.Name = "bt53";
            this.bt53.Size = new System.Drawing.Size(40, 48);
            this.bt53.TabIndex = 39;
            this.bt53.UseVisualStyleBackColor = false;
            this.bt53.Click += new System.EventHandler(this.bt53_Click);
            // 
            // bt50
            // 
            this.bt50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt50.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt50.ForeColor = System.Drawing.Color.Black;
            this.bt50.Location = new System.Drawing.Point(158, 112);
            this.bt50.Name = "bt50";
            this.bt50.Size = new System.Drawing.Size(40, 48);
            this.bt50.TabIndex = 15;
            this.bt50.UseVisualStyleBackColor = false;
            this.bt50.Click += new System.EventHandler(this.bt50_Click);
            // 
            // bt52
            // 
            this.bt52.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt52.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt52.ForeColor = System.Drawing.Color.Black;
            this.bt52.Location = new System.Drawing.Point(234, 112);
            this.bt52.Name = "bt52";
            this.bt52.Size = new System.Drawing.Size(40, 48);
            this.bt52.TabIndex = 38;
            this.bt52.UseVisualStyleBackColor = false;
            this.bt52.Click += new System.EventHandler(this.bt52_Click);
            // 
            // bt49
            // 
            this.bt49.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt49.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt49.ForeColor = System.Drawing.Color.Black;
            this.bt49.Location = new System.Drawing.Point(120, 112);
            this.bt49.Name = "bt49";
            this.bt49.Size = new System.Drawing.Size(40, 48);
            this.bt49.TabIndex = 14;
            this.bt49.UseVisualStyleBackColor = false;
            this.bt49.Click += new System.EventHandler(this.bt49_Click);
            // 
            // bt51
            // 
            this.bt51.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt51.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt51.ForeColor = System.Drawing.Color.Black;
            this.bt51.Location = new System.Drawing.Point(196, 112);
            this.bt51.Name = "bt51";
            this.bt51.Size = new System.Drawing.Size(40, 48);
            this.bt51.TabIndex = 37;
            this.bt51.UseVisualStyleBackColor = false;
            this.bt51.Click += new System.EventHandler(this.bt51_Click);
            // 
            // bt40
            // 
            this.bt40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt40.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt40.ForeColor = System.Drawing.Color.Black;
            this.bt40.Location = new System.Drawing.Point(120, 66);
            this.bt40.Name = "bt40";
            this.bt40.Size = new System.Drawing.Size(40, 48);
            this.bt40.TabIndex = 9;
            this.bt40.UseVisualStyleBackColor = false;
            this.bt40.Click += new System.EventHandler(this.bt40_Click);
            // 
            // bt45
            // 
            this.bt45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt45.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt45.ForeColor = System.Drawing.Color.Black;
            this.bt45.Location = new System.Drawing.Point(310, 66);
            this.bt45.Name = "bt45";
            this.bt45.Size = new System.Drawing.Size(40, 48);
            this.bt45.TabIndex = 36;
            this.bt45.UseVisualStyleBackColor = false;
            this.bt45.Click += new System.EventHandler(this.bt45_Click);
            // 
            // bt48
            // 
            this.bt48.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt48.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt48.ForeColor = System.Drawing.Color.Black;
            this.bt48.Location = new System.Drawing.Point(82, 112);
            this.bt48.Name = "bt48";
            this.bt48.Size = new System.Drawing.Size(40, 48);
            this.bt48.TabIndex = 13;
            this.bt48.UseVisualStyleBackColor = false;
            this.bt48.Click += new System.EventHandler(this.bt48_Click);
            // 
            // bt44
            // 
            this.bt44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt44.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt44.ForeColor = System.Drawing.Color.Black;
            this.bt44.Location = new System.Drawing.Point(272, 66);
            this.bt44.Name = "bt44";
            this.bt44.Size = new System.Drawing.Size(40, 48);
            this.bt44.TabIndex = 35;
            this.bt44.UseVisualStyleBackColor = false;
            this.bt44.Click += new System.EventHandler(this.bt44_Click);
            // 
            // bt47
            // 
            this.bt47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt47.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt47.ForeColor = System.Drawing.Color.Black;
            this.bt47.Location = new System.Drawing.Point(44, 112);
            this.bt47.Name = "bt47";
            this.bt47.Size = new System.Drawing.Size(40, 48);
            this.bt47.TabIndex = 12;
            this.bt47.UseVisualStyleBackColor = false;
            this.bt47.Click += new System.EventHandler(this.bt47_Click);
            // 
            // bt43
            // 
            this.bt43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt43.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt43.ForeColor = System.Drawing.Color.Black;
            this.bt43.Location = new System.Drawing.Point(234, 66);
            this.bt43.Name = "bt43";
            this.bt43.Size = new System.Drawing.Size(40, 48);
            this.bt43.TabIndex = 34;
            this.bt43.UseVisualStyleBackColor = false;
            this.bt43.Click += new System.EventHandler(this.bt43_Click);
            // 
            // bt46
            // 
            this.bt46.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt46.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt46.ForeColor = System.Drawing.Color.Black;
            this.bt46.Location = new System.Drawing.Point(6, 112);
            this.bt46.Name = "bt46";
            this.bt46.Size = new System.Drawing.Size(40, 48);
            this.bt46.TabIndex = 11;
            this.bt46.UseVisualStyleBackColor = false;
            this.bt46.Click += new System.EventHandler(this.bt46_Click);
            // 
            // bt42
            // 
            this.bt42.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt42.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt42.ForeColor = System.Drawing.Color.Black;
            this.bt42.Location = new System.Drawing.Point(196, 66);
            this.bt42.Name = "bt42";
            this.bt42.Size = new System.Drawing.Size(40, 48);
            this.bt42.TabIndex = 33;
            this.bt42.UseVisualStyleBackColor = false;
            this.bt42.Click += new System.EventHandler(this.bt42_Click);
            // 
            // bt41
            // 
            this.bt41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt41.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt41.ForeColor = System.Drawing.Color.Black;
            this.bt41.Location = new System.Drawing.Point(158, 66);
            this.bt41.Name = "bt41";
            this.bt41.Size = new System.Drawing.Size(40, 48);
            this.bt41.TabIndex = 10;
            this.bt41.UseVisualStyleBackColor = false;
            this.bt41.Click += new System.EventHandler(this.bt41_Click);
            // 
            // bt36
            // 
            this.bt36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt36.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt36.ForeColor = System.Drawing.Color.Black;
            this.bt36.Location = new System.Drawing.Point(310, 20);
            this.bt36.Name = "bt36";
            this.bt36.Size = new System.Drawing.Size(40, 48);
            this.bt36.TabIndex = 32;
            this.bt36.UseVisualStyleBackColor = false;
            this.bt36.Click += new System.EventHandler(this.bt36_Click);
            // 
            // bt39
            // 
            this.bt39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt39.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt39.ForeColor = System.Drawing.Color.Black;
            this.bt39.Location = new System.Drawing.Point(82, 66);
            this.bt39.Name = "bt39";
            this.bt39.Size = new System.Drawing.Size(40, 48);
            this.bt39.TabIndex = 8;
            this.bt39.UseVisualStyleBackColor = false;
            this.bt39.Click += new System.EventHandler(this.bt39_Click);
            // 
            // bt35
            // 
            this.bt35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt35.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt35.ForeColor = System.Drawing.Color.Black;
            this.bt35.Location = new System.Drawing.Point(272, 20);
            this.bt35.Name = "bt35";
            this.bt35.Size = new System.Drawing.Size(40, 48);
            this.bt35.TabIndex = 31;
            this.bt35.UseVisualStyleBackColor = false;
            this.bt35.Click += new System.EventHandler(this.bt35_Click);
            // 
            // bt38
            // 
            this.bt38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt38.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt38.ForeColor = System.Drawing.Color.Black;
            this.bt38.Location = new System.Drawing.Point(44, 66);
            this.bt38.Name = "bt38";
            this.bt38.Size = new System.Drawing.Size(40, 48);
            this.bt38.TabIndex = 7;
            this.bt38.UseVisualStyleBackColor = false;
            this.bt38.Click += new System.EventHandler(this.bt38_Click);
            // 
            // bt34
            // 
            this.bt34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt34.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt34.ForeColor = System.Drawing.Color.Black;
            this.bt34.Location = new System.Drawing.Point(234, 20);
            this.bt34.Name = "bt34";
            this.bt34.Size = new System.Drawing.Size(40, 48);
            this.bt34.TabIndex = 30;
            this.bt34.UseVisualStyleBackColor = false;
            this.bt34.Click += new System.EventHandler(this.bt34_Click);
            // 
            // bt37
            // 
            this.bt37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt37.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt37.ForeColor = System.Drawing.Color.Black;
            this.bt37.Location = new System.Drawing.Point(6, 66);
            this.bt37.Name = "bt37";
            this.bt37.Size = new System.Drawing.Size(40, 48);
            this.bt37.TabIndex = 6;
            this.bt37.UseVisualStyleBackColor = false;
            this.bt37.Click += new System.EventHandler(this.bt37_Click);
            // 
            // bt33
            // 
            this.bt33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt33.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt33.ForeColor = System.Drawing.Color.Black;
            this.bt33.Location = new System.Drawing.Point(196, 20);
            this.bt33.Name = "bt33";
            this.bt33.Size = new System.Drawing.Size(40, 48);
            this.bt33.TabIndex = 29;
            this.bt33.UseVisualStyleBackColor = false;
            this.bt33.Click += new System.EventHandler(this.bt33_Click);
            // 
            // bt32
            // 
            this.bt32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt32.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt32.ForeColor = System.Drawing.Color.Black;
            this.bt32.Location = new System.Drawing.Point(158, 20);
            this.bt32.Name = "bt32";
            this.bt32.Size = new System.Drawing.Size(40, 48);
            this.bt32.TabIndex = 5;
            this.bt32.UseVisualStyleBackColor = false;
            this.bt32.Click += new System.EventHandler(this.bt32_Click);
            // 
            // bt31
            // 
            this.bt31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt31.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt31.ForeColor = System.Drawing.Color.Black;
            this.bt31.Location = new System.Drawing.Point(120, 20);
            this.bt31.Name = "bt31";
            this.bt31.Size = new System.Drawing.Size(40, 48);
            this.bt31.TabIndex = 4;
            this.bt31.UseVisualStyleBackColor = false;
            this.bt31.Click += new System.EventHandler(this.bt31_Click);
            // 
            // bt30
            // 
            this.bt30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt30.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt30.ForeColor = System.Drawing.Color.Black;
            this.bt30.Location = new System.Drawing.Point(82, 20);
            this.bt30.Name = "bt30";
            this.bt30.Size = new System.Drawing.Size(40, 48);
            this.bt30.TabIndex = 3;
            this.bt30.UseVisualStyleBackColor = false;
            this.bt30.Click += new System.EventHandler(this.bt30_Click);
            // 
            // bt29
            // 
            this.bt29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt29.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt29.ForeColor = System.Drawing.Color.Black;
            this.bt29.Location = new System.Drawing.Point(44, 20);
            this.bt29.Name = "bt29";
            this.bt29.Size = new System.Drawing.Size(40, 48);
            this.bt29.TabIndex = 2;
            this.bt29.UseVisualStyleBackColor = false;
            this.bt29.Click += new System.EventHandler(this.bt29_Click);
            // 
            // gbx1
            // 
            this.gbx1.Controls.Add(this.bt1);
            this.gbx1.Controls.Add(this.bt27);
            this.gbx1.Controls.Add(this.bt26);
            this.gbx1.Controls.Add(this.bt25);
            this.gbx1.Controls.Add(this.bt24);
            this.gbx1.Controls.Add(this.bt18);
            this.gbx1.Controls.Add(this.bt17);
            this.gbx1.Controls.Add(this.bt16);
            this.gbx1.Controls.Add(this.bt15);
            this.gbx1.Controls.Add(this.bt9);
            this.gbx1.Controls.Add(this.bt8);
            this.gbx1.Controls.Add(this.bt7);
            this.gbx1.Controls.Add(this.bt6);
            this.gbx1.Controls.Add(this.bt23);
            this.gbx1.Controls.Add(this.bt22);
            this.gbx1.Controls.Add(this.bt13);
            this.gbx1.Controls.Add(this.bt21);
            this.gbx1.Controls.Add(this.bt20);
            this.gbx1.Controls.Add(this.bt19);
            this.gbx1.Controls.Add(this.bt14);
            this.gbx1.Controls.Add(this.bt12);
            this.gbx1.Controls.Add(this.bt11);
            this.gbx1.Controls.Add(this.bt10);
            this.gbx1.Controls.Add(this.bt5);
            this.gbx1.Controls.Add(this.bt4);
            this.gbx1.Controls.Add(this.bt3);
            this.gbx1.Controls.Add(this.bt2);
            this.gbx1.Location = new System.Drawing.Point(0, 0);
            this.gbx1.Name = "gbx1";
            this.gbx1.Size = new System.Drawing.Size(356, 164);
            this.gbx1.TabIndex = 16;
            this.gbx1.TabStop = false;
            this.gbx1.Text = "Cartão 1";
            this.gbx1.Visible = false;
            this.gbx1.Enter += new System.EventHandler(this.groupBox3_Enter_1);
            // 
            // bt1
            // 
            this.bt1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt1.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt1.ForeColor = System.Drawing.Color.Black;
            this.bt1.Location = new System.Drawing.Point(6, 20);
            this.bt1.Name = "bt1";
            this.bt1.Size = new System.Drawing.Size(40, 48);
            this.bt1.TabIndex = 29;
            this.bt1.UseVisualStyleBackColor = false;
            this.bt1.Click += new System.EventHandler(this.bt1_Click);
            // 
            // bt27
            // 
            this.bt27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt27.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt27.ForeColor = System.Drawing.Color.Black;
            this.bt27.Location = new System.Drawing.Point(310, 112);
            this.bt27.Name = "bt27";
            this.bt27.Size = new System.Drawing.Size(40, 48);
            this.bt27.TabIndex = 28;
            this.bt27.UseVisualStyleBackColor = false;
            this.bt27.Click += new System.EventHandler(this.bt27_Click_1);
            // 
            // bt26
            // 
            this.bt26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt26.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt26.ForeColor = System.Drawing.Color.Black;
            this.bt26.Location = new System.Drawing.Point(272, 112);
            this.bt26.Name = "bt26";
            this.bt26.Size = new System.Drawing.Size(40, 48);
            this.bt26.TabIndex = 27;
            this.bt26.UseVisualStyleBackColor = false;
            this.bt26.Click += new System.EventHandler(this.bt26_Click_1);
            // 
            // bt25
            // 
            this.bt25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt25.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt25.ForeColor = System.Drawing.Color.Black;
            this.bt25.Location = new System.Drawing.Point(234, 112);
            this.bt25.Name = "bt25";
            this.bt25.Size = new System.Drawing.Size(40, 48);
            this.bt25.TabIndex = 26;
            this.bt25.UseVisualStyleBackColor = false;
            this.bt25.Click += new System.EventHandler(this.bt25_Click_1);
            // 
            // bt24
            // 
            this.bt24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt24.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt24.ForeColor = System.Drawing.Color.Black;
            this.bt24.Location = new System.Drawing.Point(196, 112);
            this.bt24.Name = "bt24";
            this.bt24.Size = new System.Drawing.Size(40, 48);
            this.bt24.TabIndex = 25;
            this.bt24.UseVisualStyleBackColor = false;
            this.bt24.Click += new System.EventHandler(this.bt24_Click_1);
            // 
            // bt18
            // 
            this.bt18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt18.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt18.ForeColor = System.Drawing.Color.Black;
            this.bt18.Location = new System.Drawing.Point(310, 66);
            this.bt18.Name = "bt18";
            this.bt18.Size = new System.Drawing.Size(40, 48);
            this.bt18.TabIndex = 24;
            this.bt18.UseVisualStyleBackColor = false;
            this.bt18.Click += new System.EventHandler(this.bt18_Click_1);
            // 
            // bt17
            // 
            this.bt17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt17.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt17.ForeColor = System.Drawing.Color.Black;
            this.bt17.Location = new System.Drawing.Point(272, 66);
            this.bt17.Name = "bt17";
            this.bt17.Size = new System.Drawing.Size(40, 48);
            this.bt17.TabIndex = 23;
            this.bt17.UseVisualStyleBackColor = false;
            this.bt17.Click += new System.EventHandler(this.bt17_Click_1);
            // 
            // bt16
            // 
            this.bt16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt16.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt16.ForeColor = System.Drawing.Color.Black;
            this.bt16.Location = new System.Drawing.Point(234, 66);
            this.bt16.Name = "bt16";
            this.bt16.Size = new System.Drawing.Size(40, 48);
            this.bt16.TabIndex = 22;
            this.bt16.UseVisualStyleBackColor = false;
            this.bt16.Click += new System.EventHandler(this.bt16_Click);
            // 
            // bt15
            // 
            this.bt15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt15.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt15.ForeColor = System.Drawing.Color.Black;
            this.bt15.Location = new System.Drawing.Point(196, 66);
            this.bt15.Name = "bt15";
            this.bt15.Size = new System.Drawing.Size(40, 48);
            this.bt15.TabIndex = 21;
            this.bt15.UseVisualStyleBackColor = false;
            this.bt15.Click += new System.EventHandler(this.bt15_Click_1);
            // 
            // bt9
            // 
            this.bt9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt9.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt9.ForeColor = System.Drawing.Color.Black;
            this.bt9.Location = new System.Drawing.Point(310, 20);
            this.bt9.Name = "bt9";
            this.bt9.Size = new System.Drawing.Size(40, 48);
            this.bt9.TabIndex = 20;
            this.bt9.UseVisualStyleBackColor = false;
            this.bt9.Click += new System.EventHandler(this.bt9_Click_1);
            // 
            // bt8
            // 
            this.bt8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt8.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt8.ForeColor = System.Drawing.Color.Black;
            this.bt8.Location = new System.Drawing.Point(272, 20);
            this.bt8.Name = "bt8";
            this.bt8.Size = new System.Drawing.Size(40, 48);
            this.bt8.TabIndex = 19;
            this.bt8.UseVisualStyleBackColor = false;
            this.bt8.Click += new System.EventHandler(this.bt8_Click_1);
            // 
            // bt7
            // 
            this.bt7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt7.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt7.ForeColor = System.Drawing.Color.Black;
            this.bt7.Location = new System.Drawing.Point(234, 20);
            this.bt7.Name = "bt7";
            this.bt7.Size = new System.Drawing.Size(40, 48);
            this.bt7.TabIndex = 18;
            this.bt7.UseVisualStyleBackColor = false;
            this.bt7.Click += new System.EventHandler(this.bt7_Click_1);
            // 
            // bt6
            // 
            this.bt6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt6.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt6.ForeColor = System.Drawing.Color.Black;
            this.bt6.Location = new System.Drawing.Point(196, 20);
            this.bt6.Name = "bt6";
            this.bt6.Size = new System.Drawing.Size(40, 48);
            this.bt6.TabIndex = 17;
            this.bt6.UseVisualStyleBackColor = false;
            this.bt6.Click += new System.EventHandler(this.bt6_Click_1);
            // 
            // bt23
            // 
            this.bt23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt23.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt23.ForeColor = System.Drawing.Color.Black;
            this.bt23.Location = new System.Drawing.Point(158, 112);
            this.bt23.Name = "bt23";
            this.bt23.Size = new System.Drawing.Size(40, 48);
            this.bt23.TabIndex = 15;
            this.bt23.UseVisualStyleBackColor = false;
            this.bt23.Click += new System.EventHandler(this.bt23_Click);
            // 
            // bt22
            // 
            this.bt22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt22.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt22.ForeColor = System.Drawing.Color.Black;
            this.bt22.Location = new System.Drawing.Point(120, 112);
            this.bt22.Name = "bt22";
            this.bt22.Size = new System.Drawing.Size(40, 48);
            this.bt22.TabIndex = 14;
            this.bt22.UseVisualStyleBackColor = false;
            this.bt22.Click += new System.EventHandler(this.bt22_Click);
            // 
            // bt13
            // 
            this.bt13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt13.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt13.ForeColor = System.Drawing.Color.Black;
            this.bt13.Location = new System.Drawing.Point(120, 66);
            this.bt13.Name = "bt13";
            this.bt13.Size = new System.Drawing.Size(40, 48);
            this.bt13.TabIndex = 9;
            this.bt13.UseVisualStyleBackColor = false;
            this.bt13.Click += new System.EventHandler(this.bt13_Click);
            // 
            // bt21
            // 
            this.bt21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt21.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt21.ForeColor = System.Drawing.Color.Black;
            this.bt21.Location = new System.Drawing.Point(82, 112);
            this.bt21.Name = "bt21";
            this.bt21.Size = new System.Drawing.Size(40, 48);
            this.bt21.TabIndex = 13;
            this.bt21.UseVisualStyleBackColor = false;
            this.bt21.Click += new System.EventHandler(this.bt21_Click_1);
            // 
            // bt20
            // 
            this.bt20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt20.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt20.ForeColor = System.Drawing.Color.Black;
            this.bt20.Location = new System.Drawing.Point(44, 112);
            this.bt20.Name = "bt20";
            this.bt20.Size = new System.Drawing.Size(40, 48);
            this.bt20.TabIndex = 12;
            this.bt20.UseVisualStyleBackColor = false;
            this.bt20.Click += new System.EventHandler(this.bt20_Click);
            // 
            // bt19
            // 
            this.bt19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt19.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt19.ForeColor = System.Drawing.Color.Black;
            this.bt19.Location = new System.Drawing.Point(6, 112);
            this.bt19.Name = "bt19";
            this.bt19.Size = new System.Drawing.Size(40, 48);
            this.bt19.TabIndex = 11;
            this.bt19.UseVisualStyleBackColor = false;
            this.bt19.Click += new System.EventHandler(this.bt19_Click);
            // 
            // bt14
            // 
            this.bt14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt14.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt14.ForeColor = System.Drawing.Color.Black;
            this.bt14.Location = new System.Drawing.Point(158, 66);
            this.bt14.Name = "bt14";
            this.bt14.Size = new System.Drawing.Size(40, 48);
            this.bt14.TabIndex = 10;
            this.bt14.UseVisualStyleBackColor = false;
            this.bt14.Click += new System.EventHandler(this.bt14_Click_1);
            // 
            // bt12
            // 
            this.bt12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt12.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt12.ForeColor = System.Drawing.Color.Black;
            this.bt12.Location = new System.Drawing.Point(82, 66);
            this.bt12.Name = "bt12";
            this.bt12.Size = new System.Drawing.Size(40, 48);
            this.bt12.TabIndex = 8;
            this.bt12.UseVisualStyleBackColor = false;
            this.bt12.Click += new System.EventHandler(this.bt12_Click);
            // 
            // bt11
            // 
            this.bt11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt11.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt11.ForeColor = System.Drawing.Color.Black;
            this.bt11.Location = new System.Drawing.Point(44, 66);
            this.bt11.Name = "bt11";
            this.bt11.Size = new System.Drawing.Size(40, 48);
            this.bt11.TabIndex = 7;
            this.bt11.UseVisualStyleBackColor = false;
            this.bt11.Click += new System.EventHandler(this.bt11_Click);
            // 
            // bt10
            // 
            this.bt10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt10.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt10.ForeColor = System.Drawing.Color.Black;
            this.bt10.Location = new System.Drawing.Point(6, 66);
            this.bt10.Name = "bt10";
            this.bt10.Size = new System.Drawing.Size(40, 48);
            this.bt10.TabIndex = 6;
            this.bt10.UseVisualStyleBackColor = false;
            this.bt10.Click += new System.EventHandler(this.bt10_Click);
            // 
            // bt5
            // 
            this.bt5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt5.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt5.ForeColor = System.Drawing.Color.Black;
            this.bt5.Location = new System.Drawing.Point(158, 20);
            this.bt5.Name = "bt5";
            this.bt5.Size = new System.Drawing.Size(40, 48);
            this.bt5.TabIndex = 5;
            this.bt5.UseVisualStyleBackColor = false;
            this.bt5.Click += new System.EventHandler(this.bt5_Click);
            // 
            // bt4
            // 
            this.bt4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt4.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt4.ForeColor = System.Drawing.Color.Black;
            this.bt4.Location = new System.Drawing.Point(120, 20);
            this.bt4.Name = "bt4";
            this.bt4.Size = new System.Drawing.Size(40, 48);
            this.bt4.TabIndex = 4;
            this.bt4.UseVisualStyleBackColor = false;
            this.bt4.Click += new System.EventHandler(this.bt4_Click);
            // 
            // bt3
            // 
            this.bt3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt3.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt3.ForeColor = System.Drawing.Color.Black;
            this.bt3.Location = new System.Drawing.Point(82, 20);
            this.bt3.Name = "bt3";
            this.bt3.Size = new System.Drawing.Size(40, 48);
            this.bt3.TabIndex = 3;
            this.bt3.UseVisualStyleBackColor = false;
            this.bt3.Click += new System.EventHandler(this.bt3_Click);
            // 
            // bt2
            // 
            this.bt2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bt2.Font = new System.Drawing.Font("Bell MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt2.ForeColor = System.Drawing.Color.Black;
            this.bt2.Location = new System.Drawing.Point(44, 20);
            this.bt2.Name = "bt2";
            this.bt2.Size = new System.Drawing.Size(40, 48);
            this.bt2.TabIndex = 2;
            this.bt2.UseVisualStyleBackColor = false;
            this.bt2.Click += new System.EventHandler(this.bt2_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button2.Location = new System.Drawing.Point(151, 33);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(98, 25);
            this.button2.TabIndex = 0;
            this.button2.Text = "New Game";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Olive;
            this.button1.Font = new System.Drawing.Font("Showcard Gothic", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Gold;
            this.button1.Location = new System.Drawing.Point(272, 437);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(154, 85);
            this.button1.TabIndex = 5;
            this.button1.Text = "Bingo";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.label1.ForeColor = System.Drawing.Color.Snow;
            this.label1.Location = new System.Drawing.Point(625, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(189, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Balls Remaining:90 of 90";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.label7.ForeColor = System.Drawing.Color.Snow;
            this.label7.Location = new System.Drawing.Point(12, 37);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 17);
            this.label7.TabIndex = 7;
            this.label7.Text = "Balance:";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.RoyalBlue;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.addMoneyToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(828, 24);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newGameToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // newGameToolStripMenuItem
            // 
            this.newGameToolStripMenuItem.Name = "newGameToolStripMenuItem";
            this.newGameToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.newGameToolStripMenuItem.Text = "New Game";
            this.newGameToolStripMenuItem.Click += new System.EventHandler(this.newGameToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.easyToolStripMenuItem,
            this.mediumToolStripMenuItem,
            this.difficultToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.optionsToolStripMenuItem.Text = "Levels";
            // 
            // easyToolStripMenuItem
            // 
            this.easyToolStripMenuItem.Name = "easyToolStripMenuItem";
            this.easyToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.easyToolStripMenuItem.Text = "Easy";
            this.easyToolStripMenuItem.Click += new System.EventHandler(this.easyToolStripMenuItem_Click);
            // 
            // mediumToolStripMenuItem
            // 
            this.mediumToolStripMenuItem.Name = "mediumToolStripMenuItem";
            this.mediumToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.mediumToolStripMenuItem.Text = "Medium";
            this.mediumToolStripMenuItem.Click += new System.EventHandler(this.mediumToolStripMenuItem_Click);
            // 
            // difficultToolStripMenuItem
            // 
            this.difficultToolStripMenuItem.Name = "difficultToolStripMenuItem";
            this.difficultToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.difficultToolStripMenuItem.Text = "Difficult";
            this.difficultToolStripMenuItem.Click += new System.EventHandler(this.difficultToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.helpToolStripMenuItem1});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(107, 22);
            this.helpToolStripMenuItem1.Text = "Help";
            this.helpToolStripMenuItem1.Click += new System.EventHandler(this.helpToolStripMenuItem1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Showcard Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(266, 76);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(165, 33);
            this.label8.TabIndex = 10;
            this.label8.Text = "Bingo1500";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Showcard Gothic", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.label9.Location = new System.Drawing.Point(293, 158);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(111, 37);
            this.label9.TabIndex = 11;
            this.label9.Text = "Prize";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Snap ITC", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.label19.Location = new System.Drawing.Point(260, 48);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(180, 31);
            this.label19.TabIndex = 12;
            this.label19.Text = "Welcome To";
            // 
            // lblsaldo
            // 
            this.lblsaldo.AutoSize = true;
            this.lblsaldo.Location = new System.Drawing.Point(89, 37);
            this.lblsaldo.Name = "lblsaldo";
            this.lblsaldo.Size = new System.Drawing.Size(44, 17);
            this.lblsaldo.TabIndex = 13;
            this.lblsaldo.Text = "500€";
            this.lblsaldo.Click += new System.EventHandler(this.lblsaldo_Click);
            // 
            // addMoneyToolStripMenuItem1
            // 
            this.addMoneyToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.add500ToolStripMenuItem,
            this.add1000ToolStripMenuItem,
            this.add1500ToolStripMenuItem});
            this.addMoneyToolStripMenuItem1.Name = "addMoneyToolStripMenuItem1";
            this.addMoneyToolStripMenuItem1.Size = new System.Drawing.Size(81, 20);
            this.addMoneyToolStripMenuItem1.Text = "Add Money";
            // 
            // add500ToolStripMenuItem
            // 
            this.add500ToolStripMenuItem.Name = "add500ToolStripMenuItem";
            this.add500ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.add500ToolStripMenuItem.Text = "Add 500€";
            this.add500ToolStripMenuItem.Click += new System.EventHandler(this.add500ToolStripMenuItem_Click);
            // 
            // add1000ToolStripMenuItem
            // 
            this.add1000ToolStripMenuItem.Name = "add1000ToolStripMenuItem";
            this.add1000ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.add1000ToolStripMenuItem.Text = "Add 1000€";
            this.add1000ToolStripMenuItem.Click += new System.EventHandler(this.add1000ToolStripMenuItem_Click);
            // 
            // add1500ToolStripMenuItem
            // 
            this.add1500ToolStripMenuItem.Name = "add1500ToolStripMenuItem";
            this.add1500ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.add1500ToolStripMenuItem.Text = "Add 1500€";
            this.add1500ToolStripMenuItem.Click += new System.EventHandler(this.add1500ToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSalmon;
            this.BackgroundImage = global::BingoDES.Properties.Resources.Blue_color_background_wallpapers_13_2560x1600;
            this.ClientSize = new System.Drawing.Size(828, 544);
            this.Controls.Add(this.lblsaldo);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Maroon;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "BingoDES";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.gbx2.ResumeLayout(false);
            this.gbx1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.GroupBox gbx1;
        private System.Windows.Forms.Button bt23;
        private System.Windows.Forms.Button bt22;
        private System.Windows.Forms.Button bt21;
        private System.Windows.Forms.Button bt20;
        private System.Windows.Forms.Button bt19;
        private System.Windows.Forms.Button bt14;
        private System.Windows.Forms.Button bt13;
        private System.Windows.Forms.Button bt12;
        private System.Windows.Forms.Button bt11;
        private System.Windows.Forms.Button bt10;
        private System.Windows.Forms.Button bt5;
        private System.Windows.Forms.Button bt4;
        private System.Windows.Forms.Button bt3;
        private System.Windows.Forms.Button bt2;
        private System.Windows.Forms.GroupBox gbx2;
        private System.Windows.Forms.Button bt50;
        private System.Windows.Forms.Button bt49;
        private System.Windows.Forms.Button bt40;
        private System.Windows.Forms.Button bt48;
        private System.Windows.Forms.Button bt47;
        private System.Windows.Forms.Button bt46;
        private System.Windows.Forms.Button bt41;
        private System.Windows.Forms.Button bt39;
        private System.Windows.Forms.Button bt38;
        private System.Windows.Forms.Button bt37;
        private System.Windows.Forms.Button bt32;
        private System.Windows.Forms.Button bt31;
        private System.Windows.Forms.Button bt30;
        private System.Windows.Forms.Button bt29;
        private System.Windows.Forms.Button bt28;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Button bt54;
        private System.Windows.Forms.Button bt53;
        private System.Windows.Forms.Button bt52;
        private System.Windows.Forms.Button bt51;
        private System.Windows.Forms.Button bt45;
        private System.Windows.Forms.Button bt44;
        private System.Windows.Forms.Button bt43;
        private System.Windows.Forms.Button bt42;
        private System.Windows.Forms.Button bt36;
        private System.Windows.Forms.Button bt35;
        private System.Windows.Forms.Button bt34;
        private System.Windows.Forms.Button bt33;
        private System.Windows.Forms.Button bt27;
        private System.Windows.Forms.Button bt26;
        private System.Windows.Forms.Button bt25;
        private System.Windows.Forms.Button bt24;
        private System.Windows.Forms.Button bt18;
        private System.Windows.Forms.Button bt17;
        private System.Windows.Forms.Button bt16;
        private System.Windows.Forms.Button bt15;
        private System.Windows.Forms.Button bt9;
        private System.Windows.Forms.Button bt8;
        private System.Windows.Forms.Button bt7;
        private System.Windows.Forms.Button bt6;
        private System.Windows.Forms.Button bt1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label lbQua;
        private System.Windows.Forms.Label lbTer;
        private System.Windows.Forms.Label lbSeg;
        private System.Windows.Forms.Label lbPri;
        private System.Windows.Forms.Label lbAtual;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ToolStripMenuItem newGameToolStripMenuItem;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ToolStripMenuItem easyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mediumToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem difficultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.Label lblsaldo;
        private System.Windows.Forms.ToolStripMenuItem addMoneyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem add500ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem add1000ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem add1500ToolStripMenuItem;
    }
}

