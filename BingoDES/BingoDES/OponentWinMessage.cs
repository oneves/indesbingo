﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BingoDES
{
    public partial class OponentWinMessage : Form
    {
        private string name;
        private string prize;
        public OponentWinMessage(string oponentName, string prize)
        {
            InitializeComponent();
            label2.Text = oponentName;
            label3.Text = "Have Won" + prize + "€";
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
