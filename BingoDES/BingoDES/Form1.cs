﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Timers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System;
using System.IO;
using System.Net;
using System.Text;


namespace BingoDES
{
    public partial class Form1 : Form
    {
        List<int> numeros;
        private System.Windows.Forms.Timer timer1;
        private string gameId;
        string gamePrize;
        int saldo = 500;
        private Boolean isFirstGame;
        private List<Button> botoesClicados;

        Dictionary<string, string> btns1 = new Dictionary<string, string>();
        Dictionary<string, string> btns2 = new Dictionary<string, string>();

        private List<string> selectedNumbCard1 { get; set; }
        private List<string> selectedNumbCard2 { get; set; }

        private List<int> guestCard { get; set; }
        private List<int> jenyCard { get; set; }
        private List<int> gucciCard { get; set; }

        string texto;

        public Form1()
        {
            InitializeComponent();
            numeros = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90 };
            botoesClicados = new List<Button>();
            guestCard = new List<int>();
            jenyCard = new List<int>();
            gucciCard = new List<int>();
            initializeCards(); 
            isFirstGame = true;
            selectedNumbCard1 = new List<string>();
            selectedNumbCard2 = new List<string>();
            easyToolStripMenuItem.BackColor = Color.Gray;
            pictureBox10.Visible = false;
            pictureBox2.Visible = false;
            label17.Visible = false;
            label14.Visible = false;
            label11.Visible = false;
            label4.Visible = false;
            pictureBox11.Visible = false;
            pictureBox3.Visible = false;
            label18.Visible = false;
            label15.Visible = false;
            label12.Visible = false;
            label5.Visible = false;

        }

        private void initializeCards() {

            var linha1 = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var linha2 = new List<int>() { 10, 11, 12, 13, 14, 15, 16, 17, 18 };
            var linha3 = new List<int>() { 19, 20, 21, 22, 23, 24, 25, 26, 27 };
            var linha4 = new List<int>() { 28, 29, 30, 31, 32, 33, 34, 35, 36 };
            var linha5 = new List<int>() { 37, 38, 39, 40, 41, 42, 43, 44, 45 };
            var linha6 = new List<int>() { 46, 47, 48, 49, 50, 51, 52, 53, 54 };

            var nC1 = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var nC2 = new List<int>() { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 };
            var nC3 = new List<int>() { 20, 21, 22, 23, 24, 25, 26, 27, 28, 29 };
            var nC4 = new List<int>() { 30, 31, 32, 33, 34, 35, 36, 37, 38, 39 };
            var nC5 = new List<int>() { 40, 41, 42, 43, 44, 45, 46, 47, 48, 49 };
            var nC6 = new List<int>() { 50, 51, 52, 53, 54, 55, 56, 57, 58, 59 };
            var nC7 = new List<int>() { 60, 61, 62, 63, 64, 65, 66, 67, 68, 69 };
            var nC8 = new List<int>() { 70, 71, 72, 73, 74, 75, 76, 77, 78, 79 };
            var nC9 = new List<int>() { 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90 };

            Random ran = new Random();
            escolheLinha(linha1, ran, nC1, nC2, nC3, nC4, nC5, nC6, nC7, nC8, nC9);
            escolheLinha(linha2, ran, nC1, nC2, nC3, nC4, nC5, nC6, nC7, nC8, nC9);
            escolheLinha(linha3, ran, nC1, nC2, nC3, nC4, nC5, nC6, nC7, nC8, nC9);
            escolheLinha(linha4, ran, nC1, nC2, nC3, nC4, nC5, nC6, nC7, nC8, nC9);
            escolheLinha(linha5, ran, nC1, nC2, nC3, nC4, nC5, nC6, nC7, nC8, nC9);
            escolheLinha(linha6, ran, nC1, nC2, nC3, nC4, nC5, nC6, nC7, nC8, nC9);

            populateOpponentsCards();
        }

        private void prepareNewGame()
        {


            lbQua.Text = "";
            lbTer.Text = "";
            lbSeg.Text = "";
            lbPri.Text = "";
            lbAtual.Text = "";

            numeros.Clear();
            populateNumeros();

            selectedNumbCard1.Clear();
            selectedNumbCard2.Clear();

            btns1.Clear();
            btns2.Clear();

            clearOponentCards();

            foreach (Control ct in gbx1.Controls) {
                ct.Text = "";

            }

            foreach (Control ct in gbx2.Controls)
            {
                ct.Text = "";

            }

            foreach (Button b in botoesClicados) {
                b.BackColor = Color.FromArgb(192, 255, 192);
            }

            botoesClicados.Clear();

            initializeCards();



        }

        private void populateNumeros() {

            if (numeros != null) {

                for (int i = 1; i < 91; i++) {
                    numeros.Add(i);
                }
            }
        }

        private void clearOponentCards()
        {
            guestCard.Clear();
            jenyCard.Clear();
            gucciCard.Clear();
            label10.Text = "";
            label11.Text = "";
            label12.Text = "";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Random sorte = new Random();
            string numb = sorteianumero(sorte);

            if(numb!=null && numb != String.Empty) { 
            
                //Calling for ofbiz service registerGame
                string endPoint = "http://localhost:8080/restcomponent/move";
                var client = new RestClient(endPoint);
                client.Method = HttpVerb.POST;

                client.PostData = "{\"number\":\"" + numb + "\",\"gameId\":\"" + gameId + "\"}";
                var json = client.MakeRequest();
            }
        }

        
        
        private String sorteianumero(Random numero)
        {
            String result = "";
            if(numeros.Count!=0)
            {
                int start2 = numero.Next(0, numeros.Count);

                var valor = numeros.ElementAt(start2);
                
                if (lbAtual.Text == "")
                {
                    lbAtual.Text = valor.ToString();
                    numeros.RemoveAt(start2);
                    result = valor.ToString();
                }
                else
                {
                    lbQua.Text = lbTer.Text;
                    lbTer.Text = lbSeg.Text;
                    lbSeg.Text = lbPri.Text;
                    lbPri.Text = lbAtual.Text;
                    lbAtual.Text = valor.ToString();
                    numeros.RemoveAt(start2);
                    result = valor.ToString();
                }
                label1.Text = "Balls Remaining: " + numeros.Count + " of 90";
            }

            verifyGuestCard(result);
            verifyJenyCard(result);
            verifyGucciCard(result);

            return result;
        }

        private void verifyGuestCard(string number) {

            int num = Int32.Parse(number);


            if (guestCard.Contains(num))
            {

                label10.Text = number;
                label10.BackColor = Color.Lime;
                guestCard.Remove(num);
                label16.Text = guestCard.Count.ToString();
            }
            else
            {
                label10.Text = number;
                label10.BackColor = Color.Red;
            }

            if (guestCard.Count == 0) {
                haveBingo("Guest32");
            }

        }

        private void verifyJenyCard(string number)
        {

            int num = Int32.Parse(number);


            if (jenyCard.Contains(num))
            {

                label11.Text = number;
                label11.BackColor = Color.Lime;
                jenyCard.Remove(num);
                label17.Text = jenyCard.Count.ToString();
            }
            else
            {
                label11.Text = number;
                label11.BackColor = Color.Red;
            }

            if (jenyCard.Count == 0 && pictureBox10.Visible==true)
            {
                haveBingo("Jeny");
            }

        }

        private void verifyGucciCard(string number)
        {

            int num = Int32.Parse(number);


            if (gucciCard.Contains(num))
            {

                label12.Text = number;
                label12.BackColor = Color.Lime;
                gucciCard.Remove(num);
                label18.Text = gucciCard.Count.ToString();
            }
            else
            {
                label12.Text = number;
                label12.BackColor = Color.Red;
            }

            if (gucciCard.Count == 0 && pictureBox11.Visible == true)
            {
                haveBingo("Gucci");
            }

        }

        private void populateOpponentsCards()
        {

            Random rnd = new Random();
            int num;
            int i = 0;
            int j = 0;
            int k = 0;

            while (i < 15)
            {
                num = rnd.Next(1, 91);

                if (!guestCard.Contains(num))
                {
                    guestCard.Add(num);
                    i++;
                }
            }

            while (j < 15)
            {
                num = rnd.Next(1, 91);

                if (!jenyCard.Contains(num))
                {
                    jenyCard.Add(num);
                    j++;
                }
            }


            while (k < 15)
            {
                num = rnd.Next(1, 91);

                if (!gucciCard.Contains(num))
                {
                    gucciCard.Add(num);
                    k++;
                }
            }

            
            label16.Text = guestCard.Count.ToString();
            label17.Text = jenyCard.Count.ToString();
            label18.Text = gucciCard.Count.ToString();
        }


        private void escolheLinha(List<int> linha, Random ran, List<int> n1, List<int> n2, List<int> n3, List<int> n4, List<int> n5, List<int> n6, List<int> n7, List<int> n8, List<int> n9)
        {

            for (int i = 1; i <= 5; i++)
            {
                int start2 = ran.Next(0, linha.Count);

                var valor = linha.ElementAt(start2);

                String botao = "bt";

                botao += valor;

                linha.RemoveAt(start2);
                if (valor == 1 || valor == 10 || valor == 19 || valor == 28 || valor == 37 || valor == 46) { texto = escolhenumero(n1, ran); }
                if (valor == 2 || valor == 11 || valor == 20 || valor == 29 || valor == 38 || valor == 47) { texto = escolhenumero(n2, ran); }
                if (valor == 3 || valor == 12 || valor == 21 || valor == 30 || valor == 39 || valor == 48) { texto = escolhenumero(n3, ran); }
                if (valor == 4 || valor == 13 || valor == 22 || valor == 31 || valor == 40 || valor == 49) { texto = escolhenumero(n4, ran); }
                if (valor == 5 || valor == 14 || valor == 23 || valor == 32 || valor == 41 || valor == 50) { texto = escolhenumero(n5, ran); }
                if (valor == 6 || valor == 15 || valor == 24 || valor == 33 || valor == 42 || valor == 51) { texto = escolhenumero(n6, ran); }
                if (valor == 7 || valor == 16 || valor == 25 || valor == 34 || valor == 43 || valor == 52) { texto = escolhenumero(n7, ran); }
                if (valor == 8 || valor == 17 || valor == 26 || valor == 35 || valor == 44 || valor == 53) { texto = escolhenumero(n8, ran); }
                if (valor == 9 || valor == 18 || valor == 27 || valor == 36 || valor == 45 || valor == 54) { texto = escolhenumero(n9, ran); }

                if (valor <= 27)
                {
                    foreach (Control ctl in gbx1.Controls)
                    {
                        if (botao.Equals(ctl.Name))
                        {
                            ctl.BackColor = Color.Honeydew;
                            ctl.Text = texto;
                            ctl.Visible = true;
                            ctl.MouseClick += mouseClickBtn1;

                            btns1.Add(ctl.Name, ctl.Text);

                            break;
                        }
                        
                    }
                }
                else {
                    foreach (Control ctl in gbx2.Controls)
                    {
                        if (botao.Equals(ctl.Name))
                        {
                            ctl.BackColor = Color.Honeydew;
                            ctl.Text = texto;
                            ctl.Visible = true;
                            ctl.MouseClick += mouseClickBtn2;

                            btns2.Add(ctl.Name, ctl.Text);

                            break;
                        }
                        
                    }
                }
            }
        }
       
        private string escolhenumero(List<int> linha1, Random ran1)
        {
            int num = ran1.Next(0, linha1.Count);
            var valor = linha1.ElementAt(num);
            linha1.RemoveAt(num);
            return valor.ToString();
        }

        private void mouseClickBtn1(object sender, MouseEventArgs e)
        {

            Control ctr = (Control)sender;

            if (ctr.Text != null && ctr.Text != "")
            {
                if (!selectedNumbCard1.Contains(ctr.Text))
                {
                    selectedNumbCard1.Add(ctr.Text);
                }
                else {
                    selectedNumbCard1.Remove(ctr.Text);
                }
            }

        }

        private void mouseClickBtn2(object sender, MouseEventArgs e)
        {

            Control ctr = (Control)sender;

            if (ctr.Text != null && ctr.Text != "")
            {

                if (!selectedNumbCard2.Contains(ctr.Text))
                {
                    selectedNumbCard2.Add(ctr.Text);
                }
                else
                {
                    selectedNumbCard2.Remove(ctr.Text);
                }
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

       

        private void button1_Click(object sender, EventArgs e)
        {
            button1.BackColor = Color.Green;
            gbx1.Visible = false;
            gbx2.Visible = false;

            haveBingo("You");

        }

        private void haveBingo(string winnerName) {

            if (gameId != null && gameId != String.Empty)
            {
                timer1.Stop();
                
                //Call OfBiz to get All moves from this game
                string endPoint = "http://localhost:8080/restcomponent/move/";
                var client = new RestClient(endPoint);
                client.Method = HttpVerb.GET;
                client.PostData = "{\"gameId\":" + "\"" + gameId + "\"" + "}";
                var json = client.MakeRequest();
                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var moves = serializer.Deserialize<List<Move>>(json);

                if (string.Equals(winnerName, "You", StringComparison.OrdinalIgnoreCase)) {
                    if (isResultOk(moves))
                    {
                        lauchWinPopup();
                    }
                    else
                    {
                        lauchLoosePopup();
                    }
                } else if (string.Equals(winnerName, "Guest32", StringComparison.OrdinalIgnoreCase)){

                    var form = new OponentWinMessage("Guest32", gamePrize);
                    form.StartPosition = FormStartPosition.CenterParent;
                    form.ShowDialog(this);

                } else if (string.Equals(winnerName, "Jeny", StringComparison.OrdinalIgnoreCase)){

                    var form = new OponentWinMessage("Jeny", gamePrize);
                    form.StartPosition = FormStartPosition.CenterParent;
                    form.ShowDialog(this);

                }
                else {

                    var form = new OponentWinMessage("Gucci", gamePrize);
                    form.StartPosition = FormStartPosition.CenterParent;
                    form.ShowDialog(this);

                }

            }
            button2.Enabled = true;
            newGameToolStripMenuItem.Enabled = true;
            easyToolStripMenuItem.Enabled = true;
            mediumToolStripMenuItem.Enabled = true;
            difficultToolStripMenuItem.Enabled = true;
            aboutToolStripMenuItem.Enabled = true;
            helpToolStripMenuItem1.Enabled = true;
        }

        private Boolean isResultOk(List<Move> moves)
        {
            Boolean result = true;

            List<String> aux = new List<String>();

            foreach (Move mv in moves) {
                if (string.Equals(mv.gameId, gameId, StringComparison.OrdinalIgnoreCase)) {
                    aux.Add(mv.number);
                }
            }

            //Verify if card1 and car2 has 15 registers (max number)
            if (selectedNumbCard1.Count == 15 || selectedNumbCard2.Count == 15)
            {
                if (selectedNumbCard1.Count == 15)
                {
                    foreach (string numb in selectedNumbCard1)
                    {
                        if (!aux.Contains(numb))
                        {
                            result = false;
                            break;
                        }
                    }
                }
                else if (selectedNumbCard2.Count == 15)
                {
                    foreach (string numb in selectedNumbCard2)
                    {
                        if (!aux.Contains(numb))
                        {
                            result = false;
                            break;
                        }
                    }

                }
                else if (selectedNumbCard1.Count != 15 && selectedNumbCard2.Count != 15) {
                    result = false;
                    
                }
            }
            else {
                result = false;
            }

            return result;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter_1(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (saldo - 100 >= 0)
            {
                saldo = saldo - 100;
                lblsaldo.Text = saldo.ToString() + "€";
                if (!isFirstGame)
                {
                    prepareNewGame();
                }
                startNewGame();
                isFirstGame = false;
                button2.Enabled = false;
                newGameToolStripMenuItem.Enabled = false;
                easyToolStripMenuItem.Enabled = false;
                mediumToolStripMenuItem.Enabled = false;
                difficultToolStripMenuItem.Enabled = false;
                aboutToolStripMenuItem.Enabled = false;
                helpToolStripMenuItem1.Enabled = false;
            }
            else {
                lauchLowBalancePopup();
            }
        }

        private void startNewGame() {
            gbx1.Visible = true;
            gbx2.Visible = true;

            if (isFirstGame)
            {
                timer1 = new System.Windows.Forms.Timer();
                timer1.Tick += new EventHandler(timer1_Tick);
                timer1.Interval = 5000; // in miliseconds
                timer1.Start();
            }
            else {
                
                timer1.Start();
            }
        

            //Calling for ofbiz service registerGame
            string endPoint = "http://localhost:8080/restcomponent/game";
            var client = new RestClient(endPoint);
            client.Method = HttpVerb.POST;
            Random rnd = new Random();
            string generatedPrize = rnd.Next(0, 1500).ToString();
            string gameName = "Bingo" + generatedPrize;
            client.PostData = "{\"gameName\":\""+ gameName +"\",\"gamePrize\":\""+ generatedPrize +"\"}";
            var json = client.MakeRequest();

            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            Game game = serializer.Deserialize<Game>(json);
            label8.Text = game.gameName;
            gameId = game.gameId;
            gamePrize = game.gamePrize;
            label2.Text = gamePrize + "€";
        }

        
        private void bt1_Click(object sender, EventArgs e)
        {
            if (bt1.Text != "") { marcaNumero(sender); }
        }
        private void bt2_Click(object sender, EventArgs e)
        {
            if (bt2.Text != "") { marcaNumero(sender); }
        }
        private void bt3_Click(object sender, EventArgs e)
        {
            if (bt3.Text != "") { marcaNumero(sender); }
        }
        private void bt4_Click(object sender, EventArgs e)
        {
            if (bt4.Text != "") { marcaNumero(sender); }
        }
        private void bt5_Click(object sender, EventArgs e)
        {
            if (bt5.Text != "") { marcaNumero(sender); }
        }
        private void bt6_Click_1(object sender, EventArgs e)
        {
            if (bt6.Text != "") { marcaNumero(sender); }
        }
        private void bt7_Click_1(object sender, EventArgs e)
        {
            if (bt7.Text != "") { marcaNumero(sender); }
        }
        private void bt8_Click_1(object sender, EventArgs e)
        {
            if (bt8.Text != "") { marcaNumero(sender); }
        }
        private void bt9_Click_1(object sender, EventArgs e)
        {
            if (bt9.Text != "") { marcaNumero(sender); }
        }
        private void bt10_Click(object sender, EventArgs e)
        {
            if (bt10.Text != "") { marcaNumero(sender); }
        }
        private void bt11_Click(object sender, EventArgs e)
        {
            if (bt11.Text != "") { marcaNumero(sender); }
        }
        private void bt12_Click(object sender, EventArgs e)
        {
            if (bt12.Text != "") { marcaNumero(sender); }
        }
        private void bt13_Click(object sender, EventArgs e)
        {
            if (bt13.Text != "") { marcaNumero(sender); }
        }
        private void bt14_Click_1(object sender, EventArgs e)
        {
            if (bt14.Text != "") { marcaNumero(sender); }
        }
        private void bt15_Click_1(object sender, EventArgs e)
        {
            if (bt15.Text != "") { marcaNumero(sender); }
        }
        private void bt16_Click(object sender, EventArgs e)
        {
            if (bt16.Text != "") { marcaNumero(sender); }
        }
        private void bt17_Click_1(object sender, EventArgs e)
        {
            if (bt17.Text != "") { marcaNumero(sender); }
        }
        private void bt18_Click_1(object sender, EventArgs e)
        {
            if (bt18.Text != "") { marcaNumero(sender); }
        } 
        private void bt19_Click(object sender, EventArgs e)
        {
            if (bt19.Text != "") { marcaNumero(sender); }
        }
        private void bt20_Click(object sender, EventArgs e)
        {
            if (bt20.Text != "") { marcaNumero(sender); }
        } 
        private void bt21_Click_1(object sender, EventArgs e)
        {
            if (bt21.Text != "") { marcaNumero(sender); }
        }
        private void bt22_Click(object sender, EventArgs e)
        {
            if (bt22.Text != "") { marcaNumero(sender); }
        }
        private void bt23_Click(object sender, EventArgs e)
        {
            if (bt23.Text != "") { marcaNumero(sender); }
        }
        private void bt24_Click_1(object sender, EventArgs e)
        {
            if (bt24.Text != "") { marcaNumero(sender); }
        }
        private void bt25_Click_1(object sender, EventArgs e)
        {
            if (bt25.Text != "") { marcaNumero(sender); }
        }
        private void bt26_Click_1(object sender, EventArgs e)
        {
            if (bt26.Text != "") { marcaNumero(sender); }
        }
        private void bt27_Click_1(object sender, EventArgs e)
        {
            if (bt27.Text != "") { marcaNumero(sender); }
        }        
        private void bt28_Click_1(object sender, EventArgs e)
        {
            if (bt28.Text != "") { marcaNumero(sender); }
        }
        private void bt29_Click(object sender, EventArgs e)
        {
            if (bt29.Text != "") { marcaNumero(sender); }
        }
        private void bt30_Click(object sender, EventArgs e)
        {
            if (bt30.Text != "") { marcaNumero(sender); }
        }
        private void bt31_Click(object sender, EventArgs e)
        {
            if (bt31.Text != "") { marcaNumero(sender); }
        }
        private void bt32_Click(object sender, EventArgs e)
        {
            if (bt32.Text != "") { marcaNumero(sender); }
        }
        private void bt33_Click(object sender, EventArgs e)
        {
            if (bt33.Text != "") { marcaNumero(sender); }
        }
        private void bt34_Click(object sender, EventArgs e)
        {
            if (bt34.Text != "") { marcaNumero(sender); }
        }
        private void bt35_Click(object sender, EventArgs e)
        {
            if (bt35.Text != "") { marcaNumero(sender); }
        }
        private void bt36_Click(object sender, EventArgs e)
        {
            if (bt36.Text != "") { marcaNumero(sender); }
        }
        private void bt37_Click(object sender, EventArgs e)
        {
            if (bt37.Text != "") { marcaNumero(sender); }
        }
        private void bt38_Click(object sender, EventArgs e)
        {
            if (bt38.Text != "") { marcaNumero(sender); }
        }
        private void bt39_Click(object sender, EventArgs e)
        {
            if (bt39.Text != "") { marcaNumero(sender); }
        }
        private void bt40_Click(object sender, EventArgs e)
        {
            if (bt40.Text != "") { marcaNumero(sender); }
        }
        private void bt41_Click(object sender, EventArgs e)
        {
            if (bt41.Text != "") { marcaNumero(sender); }
        }
        private void bt42_Click(object sender, EventArgs e)
        {
            if (bt42.Text != "") { marcaNumero(sender); }
        }
        private void bt43_Click(object sender, EventArgs e)
        {
            if (bt43.Text != "") { marcaNumero(sender); }
        }
        private void bt44_Click(object sender, EventArgs e)
        {
            if (bt44.Text != "") { marcaNumero(sender); }
        }
        private void bt45_Click(object sender, EventArgs e)
        {
            if (bt45.Text != "") { marcaNumero(sender); }
        }
        private void bt46_Click(object sender, EventArgs e)
        {
            if (bt46.Text != "") { marcaNumero(sender); }
        }
        private void bt47_Click(object sender, EventArgs e)
        {
            if (bt47.Text != "") { marcaNumero(sender); }
        }
        private void bt48_Click(object sender, EventArgs e)
        {
            if (bt48.Text != "") { marcaNumero(sender); }
        }
        private void bt49_Click(object sender, EventArgs e)
        {
            if (bt49.Text != "") { marcaNumero(sender); }
        }
        private void bt50_Click(object sender, EventArgs e)
        {
            if (bt50.Text != "") { marcaNumero(sender); }
        }
        private void bt51_Click(object sender, EventArgs e)
        {
            if (bt51.Text != "") { marcaNumero(sender); }
        }
       private void bt52_Click(object sender, EventArgs e)
        {
            if (bt52.Text != "") { marcaNumero(sender); }
        }
        private void bt53_Click(object sender, EventArgs e)
        {
            if (bt53.Text != "") { marcaNumero(sender); }
        }
        private void bt54_Click(object sender, EventArgs e)
        {
            if (bt54.Text != "") { marcaNumero(sender); }
        }

        private void marcaNumero(object sender)
        {
            Button botaoClica = sender as Button;

            if (botaoClica.BackColor == Color.Honeydew)

            {
                botaoClica.BackColor = Color.Green;
                botoesClicados.Add(botaoClica);
            }
            else
            {
                botaoClica.BackColor = Color.Honeydew;
                botoesClicados.Remove(botaoClica);
            }
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isFirstGame)
            {
                prepareNewGame();
            }
            startNewGame();
            isFirstGame = false;
            button2.Enabled = false;
            newGameToolStripMenuItem.Enabled = false;
            easyToolStripMenuItem.Enabled = false;
            mediumToolStripMenuItem.Enabled = false;
            difficultToolStripMenuItem.Enabled = false;
            aboutToolStripMenuItem.Enabled = false;
            helpToolStripMenuItem1.Enabled = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        public void lauchWinPopup() {
            var form = new WinMessage(gamePrize);
            form.StartPosition = FormStartPosition.CenterParent;
            form.ShowDialog(this);

            int prizemais = Int32.Parse(gamePrize);
            saldo = saldo + prizemais;
            lblsaldo.Text = saldo.ToString();
        }

        public void lauchLoosePopup()
        {
            var form = new LooseMessage("500");
            form.StartPosition = FormStartPosition.CenterParent;
            form.ShowDialog(this);

            int saldoActual = saldo - 500;
            saldo = saldoActual;

            if (saldoActual >= 0)
            {
                lblsaldo.Text = saldoActual.ToString()+ "€";
            }
            else
            {
                saldo = 0;
                lblsaldo.Text = "0€";
            }
        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void easyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox10.Visible = false;
            pictureBox2.Visible = false;
            label17.Visible = false;
            label14.Visible = false;
            label11.Visible = false;
            label4.Visible = false;
            pictureBox11.Visible = false;
            pictureBox3.Visible = false;
            label18.Visible = false;
            label15.Visible = false;
            label12.Visible = false;
            label5.Visible = false;
            easyToolStripMenuItem.BackColor = Color.Gray;
            mediumToolStripMenuItem.BackColor = Color.LightGray;
            difficultToolStripMenuItem.BackColor = Color.LightGray;

        }

        private void mediumToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox10.Visible = true;
            pictureBox2.Visible = true;
            label17.Visible = true;
            label14.Visible = true;
            label11.Visible = true;
            label4.Visible = true;
            pictureBox11.Visible = false;
            pictureBox3.Visible = false;
            label18.Visible = false;
            label15.Visible = false;
            label12.Visible = false;
            label5.Visible = false;
            easyToolStripMenuItem.BackColor = Color.LightGray;
            mediumToolStripMenuItem.BackColor = Color.Gray;
            difficultToolStripMenuItem.BackColor = Color.LightGray;
        }

        private void difficultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox10.Visible = true;
            pictureBox2.Visible = true;
            label17.Visible = true;
            label14.Visible = true;
            label11.Visible = true;
            label4.Visible = true;
            pictureBox11.Visible = true;
            pictureBox3.Visible = true;
            label18.Visible = true;
            label15.Visible = true;
            label12.Visible = true;
            label5.Visible = true;
            easyToolStripMenuItem.BackColor = Color.LightGray;
            mediumToolStripMenuItem.BackColor = Color.LightGray;
            difficultToolStripMenuItem.BackColor= Color.Gray;
        }

        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var form = new Ajuda();
            form.StartPosition = FormStartPosition.CenterParent;
            form.ShowDialog(this);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new About();
            form.StartPosition = FormStartPosition.CenterParent;
            form.ShowDialog(this);
        }

        private void lauchLowBalancePopup() {
            var form = new LowBalanceMessage();
            form.StartPosition = FormStartPosition.CenterParent;
            form.ShowDialog(this);
        }

        private void addMoneyToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void lblsaldo_Click(object sender, EventArgs e)
        {

        }

        private void add500ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saldo += 500;
            lblsaldo.Text = saldo.ToString() + "€";
        }

        private void add1000ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saldo += 1000;
            lblsaldo.Text = saldo.ToString() + "€";
        }

        private void add1500ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saldo += 1500;
            lblsaldo.Text = saldo.ToString()+ "€";
        }
    }

}
