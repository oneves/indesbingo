﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BingoDES
{
    class Move
    {

        public string lastUpdate { get; set; }
        public string entityName { get; set; }
        public string moveId { get; set; }
        public string createdTxStmp { get; set; }
        public string createdStmp { get; set; }
        public string delegatorName { get; set; }
        public string gameId { get; set; }
        public string number { get; set; }
        public string lastUpdateTxStamp { get; set; }
    }
}
