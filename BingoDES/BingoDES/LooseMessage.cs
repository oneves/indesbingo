﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BingoDES
{
    public partial class LooseMessage : Form
    {

        public LooseMessage(string gamePrize)
        {
            InitializeComponent();

            label3.Text = gamePrize + "€";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
